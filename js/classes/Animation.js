function Animation(animationType, obj, ctx) {
  this.animationType = animationType;

  this.obj = obj;
  this.ctx = ctx;

  this.type[this.animationType].init(this);

  setAnimationColor(this, 0, 0, 0);
}

Animation.prototype.play = function() {
  this.type[this.animationType].play(this);
  this.type[this.animationType].draw(this);
}

Animation.prototype.type = {};

Animation.prototype.type.circle = {
  init: function(animation) {
    animation.radius = animation.radius || 0;
    animation.opacity = animation.opacity || 1;
  },

  play: function(animation) {
    animation.radius += 0.1;
    animation.opacity -= 0.005;

    if (animation.opacity <= 0) {
      animation.ended = true;
      
      animation.radius = 1;
      animation.opacity = 1;
    } else {
      animation.ended = false;
    }
  },

  draw: function(animation) {
    animation.ctx.beginPath();
    animation.ctx.arc(animation.obj.x + (animation.obj.width / 2), animation.obj.y + (animation.obj.height / 2), animation.radius, 0, 2 * Math.PI, false);
    animation.ctx.lineWidth = 1;
    animation.ctx.strokeStyle = animation.color + animation.opacity  + ')';
    animation.ctx.stroke();
  }
}

function setAnimationColor(animation, r, g, b) {
  animation.color = 'rgba(' + r + ', ' + g + ', ' + b + ', ';
}
