function Block(x, type, ctx) {
  this.x = x;
  this.y = 0;

  this.width = BLOCK_WIDTH;
  this.height = BLOCK_HEIGHT;

  this.type = type;

  this.isAnimated = this.type == 'bonus';

  if (this.isAnimated) {
    this.bonusAnimation = new Animation('circle', this, ctx);
  }

  if (Math.random() > 0.4) {
      this.applyRandomForce(1);
  }
}

Block.prototype.draw = function(ctx) {
  switch (this.type) {
    case 'enemy':
      ctx.fillStyle = '#444444';
      break;
    case 'bonus':
      ctx.fillStyle = 'blue';
      break;
  }

  ctx.fillRect(this.x, this.y, this.width, this.height);

  if (this.isAnimated) {
    this.bonusAnimation.play();
  }
}

Block.prototype.applyRandomForce = function(force) {
  var dir, force;

  if (Math.random() > 0.5) {
    dir = 1;
  } else {
    dir = -1;
  }

  this.force = {
    dir: dir,
    force: force
  };
}
