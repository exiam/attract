function Player() {
  this.x = SCREEN_WIDTH / 2;
  this.y = SCREEN_HEIGHT / 2;

  this.width = PLAYER_WIDTH;
  this.height = PLAYER_HEIGHT;

  this.color = '#69D2F5';

  this.latency = 16;

  this.moving = false;
  this.destination = false;

  this.score = 0;
  
  this.godMode = false;
  this.godModeTimer = null;

  this.dead = false;
  this.deadAnimationDuration = 2000;
  this.deadAnimationTimer = 0;
}

Player.prototype.update = function(dt) {
  if (this.moving) {
    if (this.x === this.destination.x && this.y === this.destination.y) {
      this.moving = false;
    } else {
      this.move();
    }
  }

  if (this.dead && this.deadAnimationTimer < this.deadAnimationDuration) {
    this.deadAnimationTimer += 1000 * dt;
  }
}

Player.prototype.move = function() {
  var posX = this.x + (this.width / 2);
  var posY = this.y + (this.height / 2);

  var toMouseX = this.destination.x - posX;
  var toMouseY = this.destination.y - posY;

  this.x += toMouseX / this.latency;
  this.y += toMouseY / this.latency;

  this.score += Math.round(Math.abs(toMouseX / this.latency) + Math.abs(toMouseY / this.latency));
}

Player.prototype.draw = function(ctx, mouseController) {
  if (this.dead && this.deadAnimationTimer < this.deadAnimationDuration) {
    this.renderDeadAnimation(ctx);
  } else {
    ctx.fillStyle = this.color;

    if (this.godMode) {
      ctx.fillStyle = '#aa0000';
    }

    ctx.fillRect(this.x, this.y, this.width, this.height);
  
    this.drawPath(ctx, mouseController);
  }
}

Player.prototype.drawPath = function(ctx, mouseController) {
  var posX = this.x + (this.width / 2);
  var posY = this.y + (this.height / 2);

  var toMouseX = mouseController.x - posX;
  var toMouseY = mouseController.y - posY;

  ctx.beginPath();
  ctx.moveTo(posX, posY);
  ctx.lineTo(posX + toMouseX, posY + toMouseY);
  ctx.strokeStyle = '#ff0000';
  ctx.stroke();
}

Player.prototype.renderDeadAnimation = function(ctx) {
  var particleWidth = this.width / 2;
  var particleHeight = this.height / 2;
  var animationModifier = this.deadAnimationTimer / 100;

  ctx.fillStyle = this.color;

  ctx.fillRect(this.x - animationModifier, this.y - animationModifier, particleWidth, particleHeight);
  ctx.fillRect(this.x - animationModifier, (this.y + particleHeight) + animationModifier, particleWidth, particleHeight);
  ctx.fillRect((this.x + particleWidth) + animationModifier, this.y - animationModifier, particleWidth, particleHeight);
  ctx.fillRect((this.x + particleWidth) + animationModifier, (this.y + particleHeight) + animationModifier, particleWidth, particleHeight);
};

Player.prototype.onClick = function(mouseController) {
  if (this.dead) {
    return;
  }

  this.destination = {
    x: mouseController.targetX,
    y: mouseController.targetY
  };

  this.moving = true;
}

Player.prototype.hit = function(target) {
  if (((this.x > target.x && this.x < target.x + target.width) || (this.x + this.width > target.x && this.x + this.width < target.x + target.width))
      && ((this.y > target.y && this.y < target.y + target.height) || (this.y + this.height > target.y && this.y + this.height < target.y + target.height))) {
      return true;
  } else {
      return false;
  }
}
