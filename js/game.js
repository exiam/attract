(function() {
  function Game() {
    this.can = document.getElementById('game');
    this.ctx = this.can.getContext('2d');

    this.can.width = SCREEN_WIDTH;
    this.can.height = SCREEN_HEIGHT;

    this.dt = 0;
    this.lastUpdate = 0;

    Game.eventHandler = new EventHandler();
    Game.mouseController = new MouseController(this.can);
  }

  Game.prototype.start = function() {
    this.changeState('mainMenu');
    this.update();
  }

  Game.prototype.update = function() {
    requestAnimationFrame(this.update.bind(this));

    var now = window.performance.now();
    this.dt = (now - this.lastUpdate) / 1000;
    this.lastUpdate = window.performance.now();

    this.state[this.currentState].update(this);
    this.draw();
  }

  Game.prototype.draw = function() {
    this.ctx.clearRect(0, 0, this.can.width, this.can.height);

    // Background
    this.ctx.fillStyle = '#EEEEEE';
    this.ctx.fillRect(0, 0, this.can.width, this.can.height);

    this.state[this.currentState].draw(this);
  }

  Game.prototype.changeState = function(newState) {
    if (this.currentState) {
      if (this.state[this.currentState].specificEvents) {
        var specificEvents = this.state[this.currentState].specificEvents;

        for (var i = 0 ; i < specificEvents.length ; i++) {
          var eventId = specificEvents[i];
          Game.eventHandler.removeEventListener(eventId);
        }

        this.state[this.currentState].specificEvents = [];
      }
    }

    this.currentState = newState;
    this.state[this.currentState].init(this);
  }

  // STATES
  Game.prototype.state = {};

  // -- MAIN MENU
  Game.prototype.state.mainMenu = {
    init: function(game) {
      var handler = function(e) {
          game.changeState('game');
      }.bind(game);

      var eventId = Game.eventHandler.addEventListener(game.can, 'click', handler, false);
      this.specificEvents.push(eventId);
    },

    update: function(game) {

    },

    draw: function(game) {
      // Title
      game.ctx.fillStyle = '#444444';
      game.ctx.font = '32px Georgia';
      game.ctx.fillText('Attract', (SCREEN_WIDTH / 2) - 100, SCREEN_HEIGHT / 2);

      // Indication
      game.ctx.font = '16px Georgia';
      game.ctx.fillText('Click to play', (SCREEN_WIDTH / 2) + 10, SCREEN_HEIGHT / 2);
    },

    specificEvents: []
  };

  // -- GAME
  Game.prototype.state.game = {
    init: function(game) {
      game.player = new Player();

      var handler = function(e) {
          game.player.onClick(Game.mouseController);
      }.bind(game);

      var eventId = Game.eventHandler.addEventListener(game.can, 'click', handler, false);
      this.specificEvents.push(eventId);

      game.blocks = [];
      game.spawnInterval = SPAWN_INTERVAL;

      game.bonusInterval = BONUS_INTERVAL;
      game.nextBonus = game.bonusInterval;

      game.levelInterval = LEVEL_INTERVAL;
      game.nextLevel = game.levelInterval;

      game.scoreTimer = new Timer(1000);
      game.spawnTimer = new Timer(game.spawnInterval);

      this.opacity = 1;
      this.endAnimationDuration = 2000;
      this.endAnimationTimer = 0;
    },

    update: function(game) {
      if (game.player.dead) {
        this.endAnimationTimer += 1000 * game.dt;
        this.opacity = 1 - (this.endAnimationTimer / this.endAnimationDuration);

        if (this.endAnimationTimer >= this.endAnimationDuration) {
          return game.changeState('gameOver');
        }
      }

      game.scoreTimer.update();
      game.spawnTimer.update();

      if (game.scoreTimer.tick && !game.player.dead) {
        game.player.score++;
      }

      if (game.player.godMode) {
        if (game.player.godModeTimer === null) {
          game.player.godModeTimer = new Timer(PLAYER_GOD_MODE_DURATION);
        }
        
        game.player.godModeTimer.update();
        
        if (game.player.godModeTimer.tick) {
          game.player.godMode = false;
          game.player.godModeTimer = null;
        }
      }

      game.updateBlocks();

      game.player.update(game.dt);
      game.draw();
    },

    draw: function(game) {
      game.ctx.globalAlpha = this.opacity;

      // Score
      game.ctx.fillStyle = '#444444';
      game.ctx.font = '20px Georgia';
      game.ctx.fillText('Score : ' + game.player.score, 10, 25);

      // Player
      game.player.draw(game.ctx, Game.mouseController);

      // Blocks
      for (var i = 0 ; i < game.blocks.length ; i++) {
        var block = game.blocks[i];
        block.draw(game.ctx);
      }

      // Reset alpha to preserve game background.
      game.ctx.globalAlpha = 1;
    },

    specificEvents: []
  };

  Game.prototype.updateBlocks = function() {
    if (this.player.score >= this.nextBonus) {
      this.blocks.push(new Block(Math.random() * SCREEN_WIDTH, 'bonus', this.ctx));
      this.nextBonus += this.bonusInterval;
    } else {
      if (this.spawnTimer.tick) {
        this.blocks.push(new Block(Math.random() * SCREEN_WIDTH, 'enemy', this.ctx));
      }
    }

    if (this.player.score >= this.nextLevel && this.spawnInterval >= 500) {
      this.spawnInterval -= REDUCE_SPAWN_INTERVAL_BY_LEVEL;
      this.spawnTimer.interval = this.spawnInterval;
      this.spawnTimer.reset();

      this.nextLevel += this.levelInterval;
    }

    var i = this.blocks.length;

    while (i--) {
      var block = this.blocks[i];

      if (block.force) {
        block.x += block.force.force * block.force.dir;
      }

      block.y++;

      if (block.y > SCREEN_HEIGHT) {
          this.blocks.splice(i, 1);
      }

      if (block.x >= SCREEN_WIDTH) {
        block.x = 0;
      } else if (block.x <= 0 - block.width) {
        block.x = SCREEN_WIDTH;
      }

      if (this.player.hit(block) && !this.player.dead) {
        this.blocks.splice(i, 1);

        switch (block.type) {
          case 'enemy':
            if (!this.player.godMode) {
              this.player.dead = true;
            }
            break;
          case 'bonus':
            this.player.godMode = true;
            break;
        }
      }
    }
  }  

  // -- GAME OVER
  Game.prototype.state.gameOver = {
    init: function(game) {
      var handler = function(e) {
          game.changeState('game');
      }.bind(game);

      var eventId = Game.eventHandler.addEventListener(game.can, 'click', handler, false);
      this.specificEvents.push(eventId);

      this.opacity = 0;
      this.fadeInDuration = 1000;
      this.fadeInTimer = 0;
    },

    update: function(game) {
      if (this.fadeInTimer < this.fadeInDuration) {
        this.fadeInTimer += 1000 * game.dt;
        this.opacity = this.fadeInTimer / this.fadeInDuration;
      }
    },

    draw: function(game) {
      game.ctx.globalAlpha = this.opacity;

      // Title
      game.ctx.fillStyle = '#444444';
      game.ctx.font = '32px Georgia';
      game.ctx.fillText('Game Over', (SCREEN_WIDTH / 2) - 100, SCREEN_HEIGHT / 2);

      game.ctx.font = '16px Georgia';
      game.ctx.fillText('Score : ' + game.player.score, (SCREEN_WIDTH / 2) - 100, (SCREEN_HEIGHT / 2) + 25);

      game.ctx.font = '16px Georgia';
      game.ctx.fillText('Click to retry', (SCREEN_WIDTH / 2) - 100, (SCREEN_HEIGHT / 2) + 100);

      game.ctx.globalAlpha = 1;
    },

    specificEvents: []
  };


  // Load game
  var game = new Game();
  game.start();
})();
