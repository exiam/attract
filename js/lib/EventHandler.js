function EventHandler() {
  this.counter = 0;
  this.events = [];
}

EventHandler.prototype.addEventListener = function(element, type, handler, capture) {
  element.addEventListener(type, handler, capture);

  this.events.push({
    element: element,
    type: type,
    handler: handler,
    capture: capture
  });

  this.counter++;

  return this.counter;
}

EventHandler.prototype.removeEventListener = function(eventId) {
  var event = this.events[eventId - 1];
  event.element.removeEventListener(event.type, event.handler, event.capture);
}
