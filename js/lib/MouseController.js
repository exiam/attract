function MouseController(element) {
  this.x = 0;
  this.y = 0;

  this.targetX = 0;
  this.targetY = 0;

  this.element = element;

  element.addEventListener('mousemove', this.move.bind(this));
  element.addEventListener('click', this.click.bind(this));
}

MouseController.prototype.move = function(e) {
  this.x = e.clientX;
  this.y = e.clientY;
}

MouseController.prototype.click = function(e) {
  this.targetX = e.clientX;
  this.targetY = e.clientY;
}

MouseController.prototype.reset = function(e) {
  this.x = 0;
  this.y = 0;
  this.targetX = 0;
  this.targetY = 0;
}
