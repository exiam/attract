function Timer(interval) {
    this.now = 0;
    this.dt = 0;
    this.last = 0;
    this.counter = 0;

    this.interval = interval;
    this.tick = false;
}

Timer.prototype.init = function() {
    var date = new Date();
    this.now = this.last = date.getTime();
};

Timer.prototype.update = function() {
    if (this.last <= 0) {
        this.init();
    }

    var date = new Date();

    this.now = date.getTime();
    this.dt = this.now - this.last;
    this.last = date.getTime();

    this.counter += this.dt;

    if (this.counter >= this.interval) {
        this.tick = true;
        this.counter = 0;
    } else {
        this.tick = false;
    }
};

Timer.prototype.reset = function() {
    this.counter = 0;
    this.tick = false;
};
